﻿#Decode and encode text using Scytale Cipher

#Function for decrypting data using Scytale
function decrypt([string]$cipher, [int32]$numCols){
    $decrypted = ""
    $LetterArray = [System.Collections.ArrayList]::new() 
    foreach($letter in [char[]]$cipher){
        $tempL = [char]$letter -as [int]
        if($tempL -In 65..90 -or $tempL -In 97..122 -or $tempL -eq 37){
            
        }else{
            #dont add
        }

        [void]$LetterArray.add([char]$letter)
    }
    $numRows = [math]::Ceiling([double]$LetterArray.Count / [double]$numCols)
    

    for($i=0;$i -lt $numRows; $i++){
        for($j=$i;$j -lt $LetterArray.Count; $j += $numRows){
            if($LetterArray[$j] -ne "%"){
                $decrypted += $LetterArray[$j]
            }
        }
    }
    Write-Host $decrypted

}

#Function for encrypting data using Scytale
function encrypt([string]$plaintext, [int32]$numCols){
    $encrypted = ""
    $LetterArray = [System.Collections.ArrayList]::new() 
    foreach($letter in [char[]]$plaintext){
        [void]$LetterArray.add([char]$letter)
        
    }

    $numRows = [math]::Ceiling([double]$LetterArray.Count / [double]$numCols)
    while($($LetterArray.Count % $numRows) -ne 0){
        $LetterArray.Add("%")
    }
    $numRows = $LetterArray.Count / $numCols
    for($i=0; $i -lt $numCols; $i++){
        for($y=$i; $y -lt $LetterArray.Count; $y += $numCols){
            $encrypted += $LetterArray[$y]

        }
    }
    Write-Host $encrypted
    
}


#Take input from user for use in the encryption/decryption process
$code =  Read-Host -Prompt 'Input String to decipher or encrypt: '
$key = Read-Host -Prompt 'Input the key you would like to use for Scytale Cipher: '
$func =  Read-Host -Prompt 'Input the function you would like to use `e` for encrypt, or `d` for decrypt: '

$keyNum = [int32]$key
#$code = $code.ToLower();

#Simple if test to select function encrypt or decrypt. 
if([string]$func -eq "d"){
    echo "Decrypting"
    decrypt $code $keyNum
}elseif([string]$func -eq "e"){
    echo "Encrypting"
    encrypt $code $keyNum
}else{
    Write-Error "No valid function found"
}