﻿# This script pings a range of IPadresses based on the input. The input can be either IP with CIDR or IP with mask.  


#Check if the subnetmask i within the valid range. 

function ValidateSubnetMask ($strSubnetMask)
{
	$bValidMask = $true
	$arrSections = @()
	$arrSections +=$strSubnetMask.split(".")
    
	if ($arrSections.count -ne 4) {$bValidMask =$false}
	

	if ($bValidMask)
	{
		[reflection.assembly]::LoadWithPartialName("'Microsoft.VisualBasic") | Out-Null
		foreach ($item in $arrSections)
		{
			if (!([Microsoft.VisualBasic.Information]::isnumeric($item))) {$bValidMask = $false}
		}
	}
	if ($bValidMask)
	{
		foreach ($item in $arrSections)
		{
			$item = [int]$item
			if ($item -lt 0 -or $item -gt 255) {$bValidMask = $false}
		}
	}

	if ($bValidMask)
	{
		foreach ($item in $arrSections)
		{
			$binary = [Convert]::ToString($item,2)
			if ($binary.length -lt 8)
			{
				do {
				$binary = "0$binary"
				} while ($binary.length -lt 8)
			}
			$strFullBinary = $strFullBinary+$binary
		}
		if ($strFullBinary.contains("01")) {$bValidMask = $false}
		#if ($bValidMask)
		#{
		#	$strFullBinary = $strFullBinary.replace("10", "1.0")
		#	if ((($strFullBinary.split(".")).count -ne 2)) {$bValidMask = $false}
		#}Write-Host $strFullBinary
	}
	Return $bValidMask
}
# Extract subnetmask from CIDR
function Get-Subnet-Mask{
    param([string]$Subnet)
    $subnetInt = $Subnet -as [int]
    $SubnetM = ""
    for($i=0;$i -lt [int]$subnetInt;$i++){
        $SubnetM += "1"
        if($SubnetM.Length -gt 31){
            break
        }
    }

    while($SubnetM.Length -lt 32){
         $SubnetM += "0"
    }
    $counter
    $SubnetM2
    foreach($char in [char[]]$SubnetM){
        $SubnetM2 += $char
        $counter++
        if($counter -eq 8){
            $SubnetM2 += "."
            $counter = 0
        }
    }

    $SubnetMaskSplit = $SubnetM2.Split(".")
    $SubnetMask = ""
    $counter
    foreach($string in $SubnetMaskSplit){
        if($string.Length -eq 8){
            $SubnetMask += [convert]::ToInt32($string,2)
            if($counter -lt 3){
                $SubnetMask += "."
                $counter++
            }
        }
    }
    return $SubnetMask
}
# Get range of IPs from IP and subnetmask
function Get-IPrange
{
<# 
  .SYNOPSIS  
    Get the IP addresses in a range 
  .EXAMPLE 
   Get-IPrange -start 192.168.8.2 -end 192.168.8.20 
  .EXAMPLE 
   Get-IPrange -ip 192.168.8.2 -mask 255.255.255.0 
  .EXAMPLE 
   Get-IPrange -ip 192.168.8.3 -cidr 24 
#> 
 
param 
( 
  [string]$start, 
  [string]$end, 
  [string]$ip, 
  [string]$mask, 
  [int]$cidr 
) 
function IP-toINT64 () { 
  param ($ip) 
 
  $octets = $ip.split(".") 
  return [int64]([int64]$octets[0]*16777216 +[int64]$octets[1]*65536 +[int64]$octets[2]*256 +[int64]$octets[3]) 
} 
 
function INT64-toIP() { 
  param ([int64]$int) 

  return (([math]::truncate($int/16777216)).tostring()+"."+([math]::truncate(($int%16777216)/65536)).tostring()+"."+([math]::truncate(($int%65536)/256)).tostring()+"."+([math]::truncate($int%256)).tostring() )
} 


if ($ip) {$ipaddr = [Net.IPAddress]::Parse($ip)} 
if ($cidr) {$maskaddr = [Net.IPAddress]::Parse((INT64-toIP -int ([convert]::ToInt64(("1"*$cidr+"0"*(32-$cidr)),2)))) } 
if ($mask) {$maskaddr = [Net.IPAddress]::Parse($mask)} 
if ($ip) {$networkaddr = new-object net.ipaddress ($maskaddr.address -band $ipaddr.address)} 
if ($ip) {$broadcastaddr = new-object net.ipaddress (([system.net.ipaddress]::parse("255.255.255.255").address -bxor $maskaddr.address -bor $networkaddr.address))} 
 
if ($ip) { 
  $startaddr = IP-toINT64 -ip $networkaddr.ipaddresstostring 
  $endaddr = IP-toINT64 -ip $broadcastaddr.ipaddresstostring 
} else { 
  $startaddr = IP-toINT64 -ip $start 
  $endaddr = IP-toINT64 -ip $end 
} 
 
 
for ($i = $startaddr; $i -le $endaddr; $i++)
{ 
  INT64-toIP -int $i 
}

}

function FixIPRange($IP, $mask){
    $IPs = [System.Collections.ArrayList]@()
    foreach($ip in Get-IPrange -ip $IP -mask $mask){
        [void]$IPs.Add($ip)
    }
    if($IPs.Count -gt 2){
        $IPs.RemoveAt(0)
        $IPs.Reverse()
        $IPs.RemoveAt(0)
        $IPs.Reverse()
    }
    return ,$IPs
}

function Ping{
    param($IP, $mask)

    #Write-Host $IP
    #foreach($ip in Get-IPrange -ip $IP -mask $mask){
     #   if($ip.Split(".")[3] -ne 0 -and $ip.Split(".")[3] -ne 255){
      #      try { Test-Connection -ComputerName $ip -Count 1 -ErrorAction Stop;  } catch { 'No Response ' + $ip }
       # }
    #}
    $IPs = FixIPRange $IP $mask
    foreach($ip in $IPs) {
        try { Test-Connection -ComputerName $ip -Count 1 -ErrorAction Stop;  } catch { 'No Response ' + $ip }
    }
}

# Declare input values and take input from user
$IP = ""
$Subnet = ""
Write-Host 'Input IP and subnet: '
Write-Host 'Valid input formats are `ping IPv4Address/CIDR` & `ping IPv4Address SubnetMask`'
Write-Host 'Example inputs are `ping 192.168.1.14/24` or `ping 192.168.1.14 255.255.255.0`'
$input = Read-Host -Prompt 'Input'
$IPnSubnetSplit = $input.Split(' ')

# Checks if input is with CIDR or subnetmask. If it is CIDR it will use Get-Subnet-Mask function to get subnetmask. 
if($IPnSubnetSplit.Count -eq 2 -and $IPnSubnetSplit.Split('/').Count -eq 3){
    $IPnSubnetSplit = $IPnSubnetSplit.Split('/')
    $Subnet = Get-Subnet-Mask($IPnSubnetSplit[2])
    $Subnet = $Subnet[3]
    $IP = $IPnSubnetSplit[1]

}elseif($IPnSubnetSplit[2].Split('.').Count -eq 4){
    $IP = $IPnSubnetSplit[1]
    $Subnet = $IPnSubnetSplit[2]
}
# Validate IP and subnetmask
$isSubnet = ValidateSubnetMask($Subnet)
if($isSubnet -eq $false){
    Write-Error 'Subnet not in correct range: Please enter a valid subnet value.'
    exit
}
if($IP -as [IPAddress] -as [bool] -eq $false){
    Write-Error "IP Address is invalid: Please enter a valid IP address."
    exit
}
# Runs ping functionS

Ping -IP $IP -mask $Subnet