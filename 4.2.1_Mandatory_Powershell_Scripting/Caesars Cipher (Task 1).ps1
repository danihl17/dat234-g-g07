﻿# Decode and encode using Caesar Cipher method

# Function to encrypt the data using Caesar cipher. 
function encrypt([string]$cipher, [int32]$key){
    $encrypted = ""
    foreach($letterE in [char[]]$cipher){
        
        $letterENum = [char]$letterE -as [int]
        $letterDNum = $letterENum + $key
        
        if ($letterE -cmatch “^[A-Z]*$”){
            if($letterDNum -gt 90){
                $letterDNum = $letterDNum - 26
                
            }
        } else {
            if($letterDNum -gt 122){
                $letterDNum = $letterDNum - 26
            }
        }
        if($letterE -eq " "){
            [char]$letterD = [char]$letterE
        }else{
            [char]$letterD = [char]$letterDNum
        }

        if ($letterENum -in 65..90 -or $letterENum -in 97..122){
            $encrypted += [char]$letterD
        }else{
            $encrypted += [char]$letterE
        }
        
        
    }
    Write-Host $encrypted
}
# Function to decrypt using caesar cipher
function decrypt([string]$text, [int32]$key){
    $decrypted = ""
    foreach($letterD in [char[]]$text){
        
        $letterDNum = [char]$letterD -as [int]
        $letterENum = $letterDNum - $key

        if ($letterD -cmatch “^[A-Z]*$”){
            if($letterENum -lt 65){
                $letterENum = $letterENum + 26
            }
        } else {
            if($letterENum -lt 97){
                $letterENum = $letterENum + 26
            }
        }
        if($letterD -eq " "){
            [char]$letterE = [char]$letterD
        }else{
            [char]$letterE = [char]$letterENum
        }


        if ($letterDNum -in 65..90 -or $letterDNum -in 97..122){
            $decrypted += [char]$letterE
        }else{
            $decrypted += [char]$letterDNum
        }
        
    }
    Write-Host $decrypted
}
# Take input from user to use in the encryption/decryption process
$code =  Read-Host -Prompt 'Input String to decipher or encrypt: '
$key = Read-Host -Prompt 'Input the key you would like to use for Caesars Cipher (If none given 13 will be used): '
$func =  Read-Host -Prompt 'Input the function you would like to use `e` for encrypt, or `d` for decrypt: '

if($key){
    $keyNum = [int32]$key
}else{
    $keyNum = 13
}


#$code = $code.ToLower();

# Simple if test to select function encrypt og decrypt
if([string]$func -eq "d"){
    echo "Decrypting"
    decrypt $code $keyNum
}elseif([string]$func -eq "e"){
    echo "Encrypting"
    encrypt $code $keyNum
}else{
    echo "No valid function found"
}