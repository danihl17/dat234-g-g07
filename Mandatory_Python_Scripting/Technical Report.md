 ![Alt text](https://www.eydecluster.com/media/23620/uia-logo.png?preset=member-logo-md)

MANDATORY PYTHON SCRIPTING
11/10 2019
 
HØST 2019
DAT234-G



GRUPPE 7

JARLE N JOHNSEN

DANIEL T REISÆTER

DANIEL H LINDEMANN

 


# Innledning
 
Oppgaven mottatt bestod i å gjennomføre diverse spørringer ved bruk av Python.
Oppgaven var som følger:

* Bruke request til å besøke en link for så å printe ut kildekoden.  
* Finne ut hvor mange av linkene som bruker http og https. Må bruke regex. 
* Printe ut alle unike Top Level Domene.
* Printe ut alle unike vertsnavn for hver URL.
* Ved å bruke regex, så skal vi finne alle unike html tags..
* Se på alle linkene på links.datapor.no og printe ut tittelen på hver side. 
* Som et tillegg til oppgaven ble vi begrenset til å kun bruke Requests og Regular Expressions for å løse oppgaven.  


Ved å gjennomføre denne oppgaven vil vi få enda bedre forståelse for bruk av requests i python og hvor mye ulike muligheter det er med disse.
 
 
## Oppgaven ble gjennomført ved å bruke følgende:
 * Visual Studio V-1.37.1
 * Python3
# Utført arbeid
 
De ulike oppgavene ble utført ved at vi først leste dokumentasjon tilknyttet både Request og Regular Expressions.


1.      Løste denne ved å bruke get.content fra nettsiden for så å printe informasjonen.


2.      For å vise antall linker med http og https kjørte vi en findall med re, hvor vi først fant alle med kun http og så https. Tok så lengden på disse 2 forskjellige listene for å se hvor mange det var av hver.


3.       Bruker en re for å filtrere ut URLer. Bruker deretter stringoperasjoner på hver URL til å hente ut TLD.


4.      Bruker en re for å filtrere ut alle URLer. Bruker stringoperasjoner for å finne hostname, ved å klippe ut alt mellom // og /. Legger de unike inn i liste og printer denne.


5.      Bruker en re for å finne alle tags. Legge disse inn i liste om de er unike og printer liste.


6.      Henter ut alle URLer. For hver URL bruker vi get.content og bruker en re for å finne titletag og printe ut verdi der.
  
# Problemer og feilsøking
 Når vi skulle finne alle http og https så brukte vi ^ og $ expressions for å finne start og slutt. Det returnerte kun null. Formaterte det som dette:”^h...s$”, altså at det skal starte på H og slutte på S. Etter litt om og men fant vi ut av at ettersom vi importerte kildekoden fra nettsiden så leter den etter start og slutt for enten hele siden, eller en linje. Dette løste vi med å søke etter https som en string, og http[^s]. 


Vi støtte på en liten utfordring på oppgave 6 når vi skulle hente ut title fra alle URLene som var på nettsiden. Om en side hadde flere "<title>" tags så ville den printe ut alle for hele siden, derfor valgte vi å først fjerne alt utenom head taggen på alle urlenes source kode før vi gjorde re search på title. Dette gav oss ett resultat vi var fornøyde med. Vi har 1 mindre title enn vi har urler, men det er fordi uia.instructure.com gir oss en unauthorized access token fordi den krever login. 

# Resultater
Resultatene av det vi har gjort er at vi får ut de ønskede svarene. Alt er lagt inn i en .py fil og vi kaller på de forskjellige funksjonene.  



# Referanser
 
Reitz, Kenneth.(u,å)Requests: HTTP for Humans™. Hentet fra: https://requests.kennethreitz.org/en/master/

Python Software Foundation. (u,å) re — Regular expression operations. Hentet fra: https://docs.python.org/3/library/re.html

Ronquillo, Alex. (2019, 23. januar) Python’s Requests Library (Guide). Hentet fra: https://realpython.com/python-requests/

w3schools(u,å)Python RegEx. Hentet fra: https://www.w3schools.com/python/python_regex.asp
 




