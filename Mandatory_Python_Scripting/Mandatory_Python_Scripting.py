import re
import requests

datapor = 'https://links.datapor.no'

def get_content(url:str):
	source = requests.get(url)
	content = source.content
	return(content)


def task_1():
	print("Task 1: \n\n")
	content = str(get_content(datapor))

	print(content)
	start()

def task_2():
	print("Task 2: \n\n")
	content = str(get_content(datapor))
	httpMatch = re.findall('http[s]', content)
	httpsMatch = re.findall('https', content)

	
	httpsLen = len(httpsMatch)
	httpLen = len(httpMatch)
	print (f'There are {httpLen} links using http.')
	print (f'There are {httpsLen} links using https.\n')

	start()

def task_3():
	print("Task 3: \n\n")
	content = str(get_content(datapor))
	#urls = re.findall(r"\w+://\w+\.\w+\.\w+/?[\w\.\?=#]*", content)
	urls = re.findall(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", content)
	
	tlds = []
	for url in urls:
		print (url)
		
		#url = url[0] + url[1] + url[2]
		#url = list(url)
		tld = url.split('//')[1]
		tld = tld.split('/')[0]
		tld = tld.split('.')
		tld.reverse()
		tld = tld[0]
		#print(tld)
		if tld not in tlds:
			tlds.append(tld)
	print (len(urls))
	print(f"Unique TLDs are {tlds} \n")

	start()
			


def task_4():
	print("Task 4: \n\n")
	content = str(get_content(datapor))
	urls = re.findall(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", content)
	hostnames = []
	count = 0
	for url in urls:
		h = url.split('//')[1]
		hostname = h.split('/')[0]
		#print (hostname)	
		count += 1
		if hostname not in hostnames:
			hostnames.append(hostname)
	print(f"Unique hostnames are {hostnames} \n")
	
	start()

def task_5():
	print("Task 5: \n\n")
	content = str(get_content(datapor))
	#tags = re.findall(r"(?<=</?)([^ >/]+)", content)
	tags = re.findall(r"</?([^ >/]+)", content)
	tagsArr = []
	for tag in tags:
		if tag not in tagsArr and tag != "!DOCTYPE":
			tagsArr.append(tag)
	print (f"Unique HTML tags are {tagsArr} \n")

	start()



def task_6():
	print("Task 6: \n\n")
	content = str(get_content(datapor))
	head = re.findall(r"<title.*?>(.*?)<\/title>*", content)
	content = content.split('<body>')[1]
	urls = re.findall(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", content)
	
	titles = []
	print (datapor.ljust(60, " "), head[0])
	for url in urls:
		contentTemp = str(get_content(url))
		h = contentTemp.split('<head>')
		
		if isinstance(h, list) and len(h) > 1:
			h = h[1].split('</head>')
			if len(h) > 0:
				h = h[0]
		else:
			h = h[0]
		
		headTemp = re.findall(r"<(title|TITLE).*?>(.*?)<\/(title|TITLE)>*", h)
		if not headTemp:
			headTemp = re.findall(r"<meta.*?name=(\"|\')(title|Title|TITLE)(\"|\').*?>(.*?)<\/title>*",h)
			print("",headTemp, url)
			print(contentTemp)
		
		for title in headTemp:
			titles.append(title[1])
			print (url.ljust(60, " "), title[1])
	
	print ('\n\n')
	print ('All the site titles: \n')
	print (titles)

	start()

def start():
	print('\n\n')
	print("Please input the task you would like to run from 1-6. Example input:1 ")
	print("To quit, type anything except the numbers 1-6.")
	ar = input('>>> ')

	q = True
	for x in range (1,7):
		if str(x) == ar:
			q = False

	if q:
		exit()
		print('kreftfaen')

	ar = int(ar)
	if ar == 1:
		print(str(task_1()) + '\n')
	elif ar == 2:
		task_2()
	elif ar == 3:
		task_3()
	elif ar == 4:
		task_4()
	elif ar == 5:
		task_5()
	elif ar == 6:
		task_6()
	else:
		exit


if __name__ == "__main__":
	start()

